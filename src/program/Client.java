/*
 * Innlevering 1
 * H�kon Martin Eide.2.klasse programmering 2012
 * 10,09.2012
 */

package program;

import java.io.*;

public class Client {


    public static void main(String[] args) {

		DBHandler handler = new DBHandler("JavaDevUser", "JavaDev");
		makeFirstChoice(handler);
		
		handler.close();
	}
	/*
	 * Will promt the user for what it wants to do.
	 * 1. Copy File
	 * 2. Read a table
	 * 3. exit.
	 * Depending on the choices it will call different helper methods.
	 * All branches will always end in a new call to makeFirstChoice and therefore it is the only exit point
	 * back to main in the program.
	 */
	private static void makeFirstChoice(DBHandler handler) {
		BufferedReader inputReader = new BufferedReader(new InputStreamReader(
				System.in));
		System.out
				.println("Hi and welcome to the DB handler.\nWhat do you want to do?");
		System.out.println("1.Copy a file\n2.Read a table\n3. Exit");
		try {
			String input = inputReader.readLine();
			if (input.equals("1")) {
				copyAFile(inputReader, handler);
			} else if (input.equals("2")) {
				readATable(inputReader, handler);
			} else if (input.equals("3")){
				System.out.println("Thank you for trying out DBHandler");
				//return;
			} else {
				makeFirstChoice(handler);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/*
	 * Asks the user for information regarding the file it wants to copy.
	 * It will do the first errorcheck for if the file exists here.
	 * If it does not it will skip calls to the DBHandler entirely and return to
	 * makeFirstChoice.
	 */
	private static void copyAFile(BufferedReader inputReader, DBHandler handler) throws IOException {

		System.out.println("What is the name of the file you want to copy?");
		String fileName = inputReader.readLine();
		System.out.println("What is the table you want to upload it in?");
		String tableName = inputReader.readLine();
		File testFile = new File(fileName);
		if (testFile.exists()){
			handler.copyFile(fileName, tableName);
			makeFirstChoice(handler);
		} else
		{
			System.out.println("That file does not exists. \nPlease try again.");
			copyAFile(inputReader, handler);
		}
	}
	/*
	 * Asks the user for what table it wants to read data from.
	 * The DBHandler does the rest.
	 */
	private static void readATable(BufferedReader inputReader, DBHandler handler) throws IOException {
		System.out.println("What is the name of the table you want to read?");
		String tableName = inputReader.readLine();
		System.out.println("Showing table: " + tableName);
		handler.showTable(tableName);
		System.out.println();
		makeFirstChoice(handler);
	}
}
