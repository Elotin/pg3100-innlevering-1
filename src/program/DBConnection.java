/*
 * Innlevering 1
 * H�kon Martin Eide.2.klasse programmering 2012
 * 10,09.2012
 */
package program;

import java.sql.*;

public class DBConnection {
	
	private String brukernavn;
	private String passord;
	//DatabaseURL is the base url to the database server, without the database name appended onto it.
	private String databaseURL;
	//The name of the database.
	private String databaseName = "JavaDev";
	private Connection databaseConnection;
	
	/*
	 * Default constructor. Will use the preassigned database for logging in.
	 * Will assume you always want to create a connection if you create an instance of this class
	 * and will therefore connect right away.
	 */
	public DBConnection(String brukernavn, String passord) 
	{
		this.brukernavn = brukernavn;
		this.passord = passord;
		databaseURL = "jdbc:mysql://hmeide.com/" + databaseName;
		ConnectToTheDatabase();
	}
	//Gives you the option to assign a database other than the default
	public DBConnection(String brukernavn, String passord, String databaseURL)
	{
		this(brukernavn, passord);
		this.databaseURL = databaseURL;
	}
	public DBConnection(String brukernavn, String passord, String databaseURL, String databaseName)
	{
		this(brukernavn, passord, databaseURL);
		this.setDatabaseName(databaseName);
	}
	public Connection getDatabaseConnection() {
		return databaseConnection;
	}
	
	
	public void closeDatabaseConnection() 
	{
		try {
			databaseConnection.close();
			System.out.println("DB connection closed");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Failed to close database connection");
		}
	}
	private void ConnectToTheDatabase()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			System.out.println("com.mysql.jdbc.driver not found");
		}
		try {
			databaseConnection = DriverManager.getConnection(databaseURL, brukernavn, passord);
			System.out.println("Connection established");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Connection failed");
		} catch (NullPointerException e)
		{
			System.out.println("null pointer");
		}
	}
	String getDatabaseName() {
		return databaseName;
	}
	void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
}
