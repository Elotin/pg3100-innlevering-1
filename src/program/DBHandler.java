/*
 * Innlevering 1
 * H�kon Martin Eide.2.klasse programmering 2012
 * 10,09.2012
 */

package program;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;

public class DBHandler {

    private DBConnection db;
    private String brukernavn;
    private String passord;
    private Connection con;

    public DBHandler(String brukernavn, String passord) {
        this.brukernavn = brukernavn;
        this.passord = passord;
        db = new DBConnection(this.brukernavn, this.passord);
        con = db.getDatabaseConnection();
    }

    /*
      * Reads a file with a certain format and creates a
      * table with the info in that file.
      * The format is as follows.
      * collumn name
      * collumn type
      * collumn lenght
      * and '/' is the seperator.
      */
    public void copyFile(String filename, String tableName) {
        String[] columns, types, lengths;

        //Will try to read from the file with the name in filename,
        try {
            BufferedReader textFileInput = new BufferedReader(new FileReader(new File(filename)));

            //Here i both read the line and split it into an array using the string.split method.
            columns = textFileInput.readLine().split("/");
            types = textFileInput.readLine().split("/");
            lengths = textFileInput.readLine().split("/");


            /*
                * A very basic formatting check of the file by checking to see if the
                * input file is ok.
                */
            if (columns.length != types.length || types.length != lengths.length) {
                System.out.println("The file was formated impropperly.");
                return;
            }
            /*
                * Another basic check to see that the datatypes specified on line 2 are valid.
                */
            for (String string : types) {
                if (!string.equalsIgnoreCase("string") && !string.equalsIgnoreCase("int")) {
                    System.out.println("The file had a wrong datatype on line to" +
                            ". The violating type is: " + string);
                    return;
                }
            }

            createTable(tableName, columns, types, lengths);

            PreparedStatement prepStatement;

            /*
                * Here we create the prepared sql string that will dynamicly update the ammount of input fields depending on the first
                */
            String prepSql = "Insert into " + tableName + " VALUES ( ";
            for (String column : columns) {
                prepSql += "?,";
            }
            /*
                * Removes the last comma and adds a closing bracket and semicolon
                */
            prepSql = prepSql.substring(0, prepSql.length() - 1);
            prepSql += ");";
            prepStatement = con.prepareStatement(prepSql);


            /*
                * From here on is the part where we start updating the table
                */

            //Current line is used to give the user an indication of where the error may be
            int currentLine = 4;
            //Sjekker at det er mer igjen i bufferen.
            while (textFileInput.ready()) {
                /*
                     * Splits the next line to an array and sends it to the updateTableUsingPreparedStatement method
                     */
                String[] inputLine = textFileInput.readLine().split("/");
                if (validateInput(inputLine, types, lengths)) {
                    updateTableUsingPreparedStatement(prepStatement, inputLine, types);
                } else
                    System.out.println("You had an invalid line in your input at line: " + currentLine);
                currentLine += 1;
            }

            textFileInput.close();
            prepStatement.close();

            System.out.println(String.format("You succsessfully created the table %s" +
                    " with the data from the file %s", tableName, filename));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Text file was not found. You put a file with the following name: " + filename + "\nAre you sure this is correct?");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    /*
      * Uses the static methods in DataaseStringResultFetcher to process a ResultSet and recieve an arraylist.
      * It then iterates over the list and prints out each index.
      */
    public void showTable(String tablename) {
        Statement stmnt;
        ResultSet result;
        try {
            stmnt = con.createStatement();
            result = stmnt.executeQuery("SELECT * FROM " + tablename);
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
        ArrayList<String> tableResults = DatabaseStringResultFetcher.getResultsFromTableAsStringArrayListUsingResultSet(result);
        for (String string : tableResults) {
            System.out.println(string);
        }
    }

    /*
     * Closes the open connections.
     */
    public void close() {
        try {
            con.close();
            db.closeDatabaseConnection();
        } catch (SQLException e) {
            System.out.println("An error occured trying to close the database connection.");
            e.printStackTrace();
        }
    }
    /*
     Will update our table based on the information we got from the copyTable method.
     It will compare the inputLine and types to make sure the line is properly formatted
     and then using the prepared statement it will insert the values into the statement and execute the update.
      */
    private void updateTableUsingPreparedStatement(PreparedStatement prepStatement, String[] inputLine, String[] types) {

        /*
               * Since we validated the array we can safely cast the string to int.
               */
        try {
            for (int inputIndex = 0; inputIndex < inputLine.length; inputIndex++) {
                if (types[inputIndex].toLowerCase().equals("string")) {
                    prepStatement.setString(inputIndex + 1, inputLine[inputIndex]);
                } else {
                    prepStatement.setInt(inputIndex + 1, Integer.parseInt(inputLine[inputIndex]));
                }
            }
            prepStatement.addBatch();
            prepStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /*
    Gets passed the arrays from the 3 first lines and uses them to create a table.
     */
    private void createTable(String tableName, String[] columns, String[] types, String[] lengths) {
        Statement stmnt;
        try {
            stmnt = con.createStatement();


            //The starting point of our sql query
            String sql = "Create Table " + tableName + " (";
            /*
            * This block iterates over the arrays and puts the data into the sql query.
            * It only uses columns.lenght as we have previously checked to make sure
            * all 3 arrays are of the same length.
            */
            for (int currentInputIndex = 0; currentInputIndex < columns.length; currentInputIndex++) {
                sql += " " + columns[currentInputIndex] + " "
                        + (types[currentInputIndex].equalsIgnoreCase("string") ? "VARCHAR" : "INT") + "("
                        + lengths[currentInputIndex] + "),";
            }
            /*
            * This is necessary to remove the '),' at the end of the sql statement.
            */
            sql = sql.substring(0, sql.length() - 1);
            sql += " );";
            //stmnt.executeUpdate(sql);
            stmnt.addBatch(sql);
            stmnt.executeBatch();
            stmnt.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*
      * Validerer input ved � sjekke at hvis det er en input merket med
      * int s� vill vi kun sende inn en int.
      */
    private static boolean validateInput(String[] input, String[] format, String[] length) {

        boolean returnValue = false;

        if (input.length != format.length)
            return false;

        for (int index = 0; index < input.length; index++) {
            int inputLength = Integer.parseInt(length[index]);
            if (format[index].toLowerCase().equals("int")) {
                try {
                    //This one is only run to check if it won't cast an exception
                    //If it does not we know we can cast it later.
                    int parsedInt = Integer.parseInt(input[index]);
                    //Since the lengt of an int is determined
                    if (input[index].length() <= Integer.parseInt(length[index]))
                        returnValue = true;
                    else returnValue = false;
                } catch (Exception e) {
                    returnValue = false;
                }
            } else
            {
                returnValue = (format[index].toLowerCase().equals("string") && input[index].length() <= inputLength);

            }
        }
        return returnValue;
    }
}
