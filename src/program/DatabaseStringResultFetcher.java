/**
 * DatabaseStringResultFetcher is a helper class with static methods for converting a resultSet to
 * an ArrayList<String> or a string array. 
 * The methods are static since the class will only ever recieve a resultset, process the data in it, and return an array.
 * Therefore having to instantiate this class is pointless.
 */


package program;

import java.sql.*;
import java.util.*;

public class DatabaseStringResultFetcher {
	
	private DatabaseStringResultFetcher()
	{
		
	}
	
	/*
	 * Uses the result set given and reads out the ammount of collumns from the meta data.
	 * It then parses all the rows into an array list.
	 * index 0 in this array list will always be the column names;
	 */
	public static ArrayList<String> getResultsFromTableAsStringArrayListUsingResultSet(ResultSet res)
	{
		int ammountOfCollumns;
		ResultSetMetaData metaData;
		ArrayList<String> returnArray = new ArrayList<String>();
		
		try {
			
			metaData = res.getMetaData();
			ammountOfCollumns = metaData.getColumnCount();
			String tableResult = "";
			for (int currentIndex = 1; currentIndex <= ammountOfCollumns; currentIndex++)
			{
				tableResult = String.format(tableResult + "%-40s", metaData.getColumnName(currentIndex));	
			}
			returnArray.add(tableResult);
			
			while (res.next())
			{
				String collumnData = "";
				for (int currentIndex = 1; currentIndex <= ammountOfCollumns; currentIndex++)
				{
					collumnData += String.format("%-40s", res.getString(currentIndex));
				}
				returnArray.add(collumnData);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return returnArray;
	}
	
	/*
	 * Will return the result as a normal string if that is what you need, saves you from having to 
	 * convert it after getting it.
	 */
	public static String[] getResultsFromTableAsStringArrayUsingResultSet(ResultSet res)
	{
		return getResultsFromTableAsStringArrayListUsingResultSet(res).toArray(new String[getResultsFromTableAsStringArrayListUsingResultSet(res).size()]);
	}
	
}
